<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

@error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
@ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!
$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
$CFG->debugdisplay = 1;             // 

$CFG->dbtype    = 'mariadb';
$CFG->dblibrary = 'native';
$CFG->dbhost    = getenv('DBHOST');
$CFG->dbname    = getenv("MYSQL_DATABASE");
$CFG->dbuser    = "root";
$CFG->dbpass    = getenv('MYSQL_ROOT_PASSWORD');
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
    'dbpersist' => 0,
    'dbport' => '',
    'dbsocket' => '',
    'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot   = getenv('WWWROOT');
$CFG->dataroot  = '/var/www/moodledata';

$CFG->phpunit_dataroot = '/var/www/phpunitdata';
$CFG->phpunit_prefix = 'phpu_';

$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
